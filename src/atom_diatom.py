from math import sqrt, sin, cos, atan2, pi
from myconstants import kelvin2hartree, DATA, vdw_length, red_mass
from itertools import groupby, combinations, product
from lj import get_eps, get_C12, get_sigma
from sympy import var, lambdify
from Ham import hyper_radius, diff_func

def place_molcules(nrg, R, m1, m2, **kwargs):
    """
    work out the inital position avd velocity of colliding molecules
    """
    p1 = sqrt(2*nrg*m1*m2/(m1 + m2))
    p2 = p1

    r1 = m2*R*p2/(m1*p2 + m2*p1)
    r2 = R - r1

    r1 *= -1
    p2 *= -1

    v1 = p1/m1
    v2 = p2/m2
    return r1, v1, r2, v2

def place_atoms(R, r, m1, m2, theta):
    """
    place the atoms relative to the centre of mass of the molecule
    """
    r1 = r*m2/(m1 + m2)
    r2 = r*m1/(m1 + m2)

    dy = r*sin(theta)
    dx = r*cos(theta)

    r1_x= R + r1*cos(theta)
    r1_y= r1*sin(theta)

    r2_x= R - r2*cos(theta)
    r2_y= -r2*sin(theta)
    return r1_x, r2_x, r1_y, r2_y


def atom_diatom_initial_condition(theta, nrg, R, r, mass_0, mass_1, mass_2, **kwargs):
    """
    generate initial conditions for atom + diatom run

    returns a dict
    """
    ic = {}

    theta *= 2*pi  #convert to radians
    mass_d = mass_1 + mass_2
    mass_a = mass_0

    r_d, vel_d, r_a, vel_a = place_molcules(nrg, R, mass_d, mass_a)

    #  place the atom
    ic['q_x_0'] = r_a
    # ic['q_y_0'] = 0

    #  place the atoms in the dimer
    r1_x, r2_x, r1_y, r2_y = place_atoms(r_d, r, mass_1, mass_2, theta)
    ic['q_x_1'] = r1_x
    ic['q_y_1'] = r1_y
    ic['q_x_2'] = r2_x
    ic['q_y_2'] = r2_y

    ic['p_x_0'] = vel_a*mass_a
    ic['p_x_1'] = vel_d*mass_1
    ic['p_x_2'] = vel_d*mass_2

    return ic


def final_condition(Ham, ic, **sys_def):
    """
    create event for termination condition
    """
    t = var('t')
    hrad_func = hyper_radius(Ham.coord_dict, Ham.num, Ham.dim, **sys_def)
    hrad_func = lambdify((t, (Ham.coords)), hrad_func)
    inital_hr = hrad_func(0, Ham.create_ic(ic))
    hrad_cond = diff_func(hrad_func, inital_hr)
    hrad_cond.terminal = True
    hrad_cond.direction = +1
    return hrad_cond


def lifetime_condition(Ham, **sys_def):
    """
    create event for lifetime calculation
    """
    r0 = get_sigma(sys_def["C12_1_2"], sys_def["C6_1_2"])**2
    r0 += get_sigma(sys_def["C12_0_1"], sys_def["C6_0_1"])**2
    r0 += get_sigma(sys_def["C12_0_2"], sys_def["C6_0_2"])**2
    r0 = sqrt(r0)

    t = var('t')
    hrad_func = hyper_radius(Ham.coord_dict, Ham.num, Ham.dim, **sys_def)
    hrad_func = lambdify((t, (Ham.coords)), hrad_func)
    hrad_func = diff_func(hrad_func, r0)
    hrad_func.terminal = False
    return hrad_func


def final_diatom(Ham, sol):
    """
    return the final diatom pair
    """
    fc = dict(zip(Ham.coord_dict.keys(), sol.y[:, -1]))
    d01 = (fc["q_x_0"] - fc["q_x_1"])**2 + (fc["q_y_0"] - fc["q_y_1"])**2
    d12 = (fc["q_x_1"] - fc["q_x_2"])**2 + (fc["q_y_1"] - fc["q_y_2"])**2
    d02 = (fc["q_x_0"] - fc["q_x_2"])**2 + (fc["q_y_0"] - fc["q_y_2"])**2
    if (d01 < d12) and (d01 < d02):
        # ((diatom), atom)
        return ((0, 1), 2)
    elif (d02 < d12) and (d02 < d01):
        return ((0, 2), 1)
    elif (d12 < d01) and (d12 < d02):
        return ((1, 2), 0)


def final_ke(Ham, sol, **sys_def):
    fc = dict(zip(Ham.coord_dict.keys(), sol.y[:, -1]))
    diatom, atom = final_diatom(Ham, sol)
    atom_ke = (fc["p_x_{}".format(atom)]**2 + fc["p_y_{}".format(atom)]**2)/(2*sys_def["mass_{}".format(atom)])
    diatom_px = fc["p_x_{}".format(diatom[0])] + fc["p_x_{}".format(diatom[1])]
    diatom_py = fc["p_y_{}".format(diatom[0])] + fc["p_y_{}".format(diatom[1])]
    diatom_ke = (diatom_px**2 + diatom_py**2)/(2*(sys_def["mass_{}".format(diatom[0])] + sys_def["mass_{}".format(diatom[1])]))
    ke = atom_ke + diatom_ke
    return ke/kelvin2hartree


def final_angle(Ham, sol):
    fc = dict(zip(Ham.coord_dict.keys(), sol.y[:, -1]))
    diatom, atom = final_diatom(Ham, sol)
    angle = atan2(fc["p_y_{}".format(atom)],fc["p_x_{}".format(atom)])
    angle = angle % (2.0*pi)
    return angle/pi


def lifetime(Ham, sol):
    fc = dict(zip(Ham.coord_dict.keys(), sol.y[:, -1]))
    if len(sol.t_events[1]) > 0:
        lifetime = sol.t_events[1][-1]-sol.t_events[1][0]
    else:
        lifetime = 0
    return lifetime


def define_system(a0, i0, a1, i1, a2, i2):
    atoms = {0: {"name": a0, "isotope": i0},
             1: {"name": a1, "isotope": i1},
             2: {"name": a2, "isotope": i2},
             }
    sys_def = {}
    for k, v in atoms.items():
        sys_def["mass_{}".format(k)] = DATA["Atoms"][v["name"]][v["isotope"]]["mass"]

    for i, j in combinations(atoms.keys(), 2):
        mol = "{}{}".format(atoms[i]["name"], atoms[j]["name"])
        C6 = DATA["Molecules"][mol]["C6"]
        De = DATA["Molecules"][mol]["De"]
        mu = red_mass(sys_def["mass_{}".format(i)], sys_def["mass_{}".format(j)])
        sys_def["C6_{}_{}".format(i, j)] = C6
        sys_def["mu_{}_{}".format(i, j)] = mu
        sys_def["l_vdw_{}_{}".format(i, j)] = vdw_length(C6, mu)
        sys_def["C12_{}_{}".format(i, j)] = get_C12(C6, De)

    sys_def["offset"] = get_eps(sys_def["C12_1_2"], sys_def["C6_1_2"])
    nrg_release = (get_eps(sys_def["C12_1_2"], sys_def["C6_1_2"]) - get_eps(sys_def["C12_0_1"], sys_def["C6_0_1"]))
    # print("energy release: {}".format(nrg_release/kelvin2hartree))
    return sys_def

