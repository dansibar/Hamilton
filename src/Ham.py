"""
Generic hamiltonian propagator - as general as possible
NO units or system dependant stuff here
"""
import sympy as sp
from itertools import groupby, combinations, product
# from scipy.misc import comb
from scipy.integrate import solve_ivp
from generic import filter_output, load_yaml
import numpy as np
from pprint import pprint
from sympy.utilities.lambdify import lambdastr
from functools import reduce
from lj import lj


def hyper_radius(coords, num, dim, **sys_def):
    """
    compute the hyper radius - note currently this is just
    a termination condition
    """
    hrad = 0
    for i, j in combinations(range(num), 2):
        r2 = sum([(coords['q_{}_{}'.format(d, i)] -
                   coords['q_{}_{}'.format(d, j)])**2 for d in dim])
        hrad += r2
    return sp.sqrt(hrad)


class Hamiltonian:
    def __init__(self, num, dim, **sys_def):
        """
        initialise the hamiltonian and the canonical coordiantes
        """
        self.dim = dim  # dimension
        self.num = num  # number of particles
        self.ps, self.qs = self.create_coords(self.dim, self.num)
        self.coords = tuple(self.ps+self.qs)
        coord_dict = {i.name: i for i in self.coords}
        self.coord_dict = coord_dict
        self.T = self.T(coord_dict, num, dim, **sys_def)
        self.V = self.V(coord_dict, num, dim, **sys_def)
        self.H = self.T + self.V
        self.COM = self.COM(coord_dict, num, dim, **sys_def)
        # self.hrad = hyper_radius(coord_dict, num, dim, **sys_def)

    def create_coords(self, dim, num):
        ps = []
        for i, j in product(dim, range(num)):
            ps.append(sp.var('p_{}_{}'.format(i, j)))
        qs = []
        for i, j in product(dim, range(num)):
            qs.append(sp.var('q_{}_{}'.format(i, j)))
        return tuple(ps), tuple(qs)

    @staticmethod
    def T(coords, num, dim, **sys_def):
        """
        generic kinetic energy term
        """
        ke = 0
        for particle in range(num):
            for dimension in dim:
                ke += coords['p_{}_{}'.format(dimension, particle)]**2/(
                    2*sys_def['mass_{}'.format(particle)])
        return ke

    @staticmethod
    def V(coords, num, dim, **sys_def):
        """
        pairwise potential energy term
        """
        pot = 0
        for i, j in combinations(range(num), 2):
            # loop over pairs of particles
            c6 = sys_def['C6_{}_{}'.format(i, j)]
            c12 = sys_def['C12_{}_{}'.format(i, j)]
            r2 = sum([(coords['q_{}_{}'.format(d, i)] -
                       coords['q_{}_{}'.format(d, j)])**2 for d in dim])
            pot += lj(r2, c12, c6)
        if "offset" in sys_def:
            pot += sys_def["offset"]
        return pot

    @staticmethod
    def COM(coords, num, dim, **sys_def):
        """
        compute centre-of-mass distance
        """
        com = []
        M = 0
        for particle in range(num):
            M += sys_def['mass_{}'.format(particle)]
            pos_a = sp.Matrix([coords['q_{}_{}'.format(dimension, particle)] for
                               dimension in dim])
            com.append(pos_a*sys_def['mass_{}'.format(particle)])
        com = reduce(sp.Add, com)
        return com.norm()

    def create_ic(self, ic, default=0):
        """
        create initial condition vector from input file and ps and qs
        consistant with the order of the coords in self.coords

        if not in dict use default - avoids lots of zeros in intial conditions
        """
        return [ic.get(i.name, default) for i in self.coords]

    def prop(self, time, ic, events, rtol=1.0e-9, atol=1.0e-12):
        """
        propagate the solution as a function of time
        Variables:
            time - tmax
            ic - dictionary of coord:vals for the inital condition
            rtol - relative tolerance passed to solve_ivp
            atol - absolute tolerance passed to solve_ivp
            nrg_tol - desired energy tolerance
        """
        t = sp.var('t')  # dummy argument
        H = sp.Matrix([self.H])
        dydt = sp.Matrix(sp.BlockMatrix([[-H.jacobian(self.qs),
                                          H.jacobian(self.ps)]]))
        dydt_func = filter_output(sp.lambdify((t, (self.coords)), dydt), 0)
        # filter output is needed because dydt is a matrix but i need
        # a function which is a vector
        method="RK45"
        method = "DOP853"  #8th order RK
        y0 = self.create_ic(ic)
        sol = solve_ivp(dydt_func, (0, time), y0, rtol=rtol, atol=atol,
                        events=events, method=method)
        return sol


def rtol_func(func, val, rtol, *args, **kwargs):
    """
    simple function - given a function which returns a value
    return a function which return the fractional change with val
    """
    def inner_func(*args, **kwargs):
        return abs((func(*args, **kwargs) - val)/val) - rtol
    return inner_func


def diff_func(func, val, *args, **kwargs):
    """
    simple function - given a function which returns a value
    return a function which returns the difference between func and val
    """
    def inner_func(*args, **kwargs):
        return func(*args, **kwargs) - val
    return inner_func
