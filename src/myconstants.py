"""
for useful constants such as masses etc
"""
from scipy.constants import hbar, physical_constants, angstrom
from math import sqrt
import yaml
from os.path import dirname

SCRIPT_DIR = dirname(__file__)

TOL = 1.0e-5

bohrradius2angstrom = physical_constants["Bohr radius"][0]/angstrom
kelvin2wavenumbers = physical_constants["kelvin-inverse meter relationship"][0]/100.0
kelvin2hartree = physical_constants["kelvin-hartree relationship"][0]
au2wavenumbers = physical_constants["hartree-inverse meter relationship"][0]/100.0
wavenumbers2au = 1.0/au2wavenumbers
amu2au = 1.0/physical_constants["electron mass in u"][0]


with open('{}/data.yml'.format(SCRIPT_DIR)) as f:
    DATA = yaml.load(f, Loader=yaml.FullLoader)


# convert De to atomic units
for i in DATA["Molecules"]:
    if "De" in DATA["Molecules"][i]:
        DATA["Molecules"][i]["De"] *= wavenumbers2au

# convert mass to atomic units
for a in DATA["Atoms"].values():
    for i in a.values():
        i["mass"] *= amu2au

# def get_bfct():
#     """
#     return bfct = hbar**2/2 in units of K*AMU*Ang**2

#     similar to bfct in molscat but in kelvin not wavenumbers

#     returns a value 24.25437532030504
#     """
#     bfct = hbar**2
#     bfct *= physical_constants["joule-kelvin relationship"][0]
#     # bfct *= physical_constants["joule-inverse meter relationship"][0]/100.0
#     bfct *= physical_constants["kilogram-atomic mass unit relationship"][0]
#     bfct /= angstrom**2
#     bfct /= 2
#     return bfct


BFCT = 1/2.0  # hbar^2/2 in atomic units


def vdw_length(C6, mu):
    return pow(mu*C6/BFCT, 1.0/4)


def vdw_energy(C6, mu):
    beta = vdw_length(C6, mu)
    return BFCT/(mu*beta**2)


def get_abar(C6, mu):
    abar = vdw_length(C6, mu)
    abar *= 4.0*pi/gamma(1/4.0)**2
    return abar


def red_mass(m1, m2):
    return m1*m2/(m1 + m2)


if __name__ == "__main__":
    import pdb
    pdb.set_trace()
