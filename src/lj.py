"""
lennard jones methods
"""


def lj(r2, C12, C6):
    """
    Lennard-Jones potential r2 is r**2
    """
    return C12/r2**6 - C6/r2**3


def get_sigma(C12, C6):
    """
    return equilibrium distance give C12 and C6
    """
    return (2.0*C12/C6)**(1.0/6)


def get_C12(C6, De):
    """
    return C12 for a given C6 and De
    """
    return C6**2/(4.0*De)


def get_eps(C12, C6):
    """
    return pot depth
    """
    return C6**2/(4.0*C12)
