from generic import reduce_output, load_yaml
import yaml
from Ham import Hamilton, T, V, hyper_radius, diff_func
from pprint import pprint
from math import sin, cos, sqrt, pi
from scipy.constants import physical_constants
from utility import atom_diatom_initial_condition
import numpy as np
import pandas as pd
from myconstants import kelvin2hartree, DATA
from itertools import groupby, combinations, product
import sympy as sp
from lj import get_eps, get_C12, get_sigma

FLOAT_FORMAT = '%.5e'
hartree2kelvin = physical_constants["hartree-kelvin relationship"][0]


def final_condition(Ham, ic):
    """
    create function for termination condition
    """
    t = sp.var('t')
    hrad_func = hyper_radius(Ham.coord_dict, Ham.num, Ham.dim, **sys_def)
    hrad_func = sp.lambdify((t, (Ham.coords)), hrad_func)
    inital_hr = hrad_func(0, Ham.create_ic(ic))
    hrad_cond = diff_func(hrad_func, inital_hr)
    hrad_cond.terminal = True
    hrad_cond.direction = +1
    return hrad_cond


def final_diatom(Ham, sol):
    """
    return the final diatom pair
    """
    fc = dict(zip(Ham.coord_dict.keys(), sol.y[:, -1]))
    d01 = (fc["q_x_0"] - fc["q_x_1"])**2 + (fc["q_y_0"] - fc["q_y_1"])**2
    d12 = (fc["q_x_1"] - fc["q_x_2"])**2 + (fc["q_y_1"] - fc["q_y_2"])**2
    d02 = (fc["q_x_0"] - fc["q_x_2"])**2 + (fc["q_y_0"] - fc["q_y_2"])**2
    if (d01 < d12) and (d01 < d02):
        # ((diatom), atom)
        return ((0, 1), 2)
    elif (d02 < d12) and (d02 < d01):
        return ((0, 2), 1)
    elif (d12 < d01) and (d12 < d02):
        return ((1, 2), 0)


def final_ke(Ham, sol):
    fc = dict(zip(Ham.coord_dict.keys(), sol.y[:, -1]))
    diatom, atom = final_diatom(Ham, sol)
    atom_ke = (fc["p_x_{}".format(atom)]**2 + fc["p_y_{}".format(atom)]**2)/(2*sys_def["mass_{}".format(atom)])
    diatom_px = fc["p_x_{}".format(diatom[0])] + fc["p_x_{}".format(diatom[1])]
    diatom_py = fc["p_y_{}".format(diatom[0])] + fc["p_y_{}".format(diatom[1])]
    diatom_ke = (diatom_px**2 + diatom_py**2)/(2*(sys_def["mass_{}".format(diatom[0])] + sys_def["mass_{}".format(diatom[1])]))
    ke = atom_ke + diatom_ke
    return ke/kelvin2hartree


def define_system(atoms):
    sys_def = {}
    for k, v in atoms.items():
        sys_def["mass_{}".format(k)] = DATA["Atoms"][v["name"]][v["isotope"]]["mass"]

    for i, j in combinations(atoms.keys(), 2):
        mol = "{}{}".format(atoms[i]["name"], atoms[j]["name"])
        C6 = DATA["Molecules"][mol]["C6"]
        De = DATA["Molecules"][mol]["De"]
        sys_def["C6_{}_{}".format(i, j)] = C6
        sys_def["C12_{}_{}".format(i, j)] = get_C12(C6, De)

    sys_def["offset"] = get_eps(sys_def["C12_1_2"], sys_def["C6_1_2"])
    nrg_release = (get_eps(sys_def["C12_1_2"], sys_def["C6_1_2"]) - get_eps(sys_def["C12_0_1"], sys_def["C6_0_1"]))
    print("energy release: {}".format(nrg_release/kelvin2hartree))
    return sys_def



atoms = {0: {"name": "K", "isotope": 41},
         1: {"name": "K", "isotope": 41},
         2: {"name": "Rb", "isotope": 87},
         }

sys_def = define_system(atoms)

Ham = Hamilton(3, ["x", "y"], T, V, **sys_def)

r = get_sigma(sys_def["C12_1_2"], sys_def["C6_1_2"])
R = 1000
nrg = 1.0*kelvin2hartree

for theta in np.random.random(5):
    theta *= 2*pi
    print(theta)
    ic = atom_diatom_initial_condition(theta, nrg, R, r, **sys_def)
    fc = final_condition(Ham, ic)
    sol = Ham.prop(1.0e10, ic, events=(fc, ))
    traj = np.vstack((sol['t'], sol['y']))
    df = pd.DataFrame(traj.T, columns=('#t', ) + Ham.coords)
    df.to_csv("traj.dat", sep="\t", index=False, float_format=FLOAT_FORMAT)
    print(sol)
    print(final_diatom(Ham, sol))
    print(final_ke(Ham, sol))
