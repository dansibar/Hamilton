"""
generic functions
"""
import yaml


def filter_output(func, item, *args, **kwargs):
    """
    simple function to filter the output from existing functions

    if func returns an iterable - just return item
    """
    def inner_func(*args, **kwargs):
        return func(*args, **kwargs)[item]
    return inner_func


def load_yaml(filen):
    """
    load a yaml file and return the json object
    """
    with open(filen) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data
