"""
script for running atom + diatom calculations
"""
import sys
sys.path.append('../src')
from Ham import Hamiltonian, hyper_radius, diff_func
from math import sin, cos, sqrt, pi
from scipy.constants import physical_constants
from atom_diatom import (atom_diatom_initial_condition, define_system,
                         lifetime_condition, final_condition, final_ke,
                         final_angle, lifetime, final_diatom)
import numpy as np
import pandas as pd
from myconstants import kelvin2hartree, DATA, vdw_length, red_mass
from itertools import groupby, combinations, product
import sympy as sp
from lj import get_eps, get_C12, get_sigma
from tempfile import NamedTemporaryFile

FLOAT_FORMAT = '%.5e'
hartree2kelvin = physical_constants["hartree-kelvin relationship"][0]
au2nseconds = physical_constants["atomic unit of time"][0]*1.0e9

sys_def = define_system("K", 41, "K", 41, "Rb", 87)
Ham = Hamiltonian(3, ["x", "y"], **sys_def)
r = get_sigma(sys_def["C12_1_2"], sys_def["C6_1_2"])
R = 250
nrg = 1.0*kelvin2hartree
lc = lifetime_condition(Ham, **sys_def)

tf = NamedTemporaryFile(prefix="data_", suffix=".dat", dir=".", delete=False)
data = []
for theta in np.linspace(0.1, 2, 11):
    # theta *= 2*pi
    ic = atom_diatom_initial_condition(theta, nrg, R, r, **sys_def)
    fc = final_condition(Ham, ic, **sys_def)
    sol = Ham.prop(1.0e10, ic, events=(fc, lc))
    traj = np.vstack((sol['t'], sol['y']))
    df = pd.DataFrame(traj.T, columns=('#t', ) + Ham.coords)
    df.to_csv("traj.dat", sep="\t", index=False, float_format=FLOAT_FORMAT)
    data.append([theta, final_ke(Ham, sol, **sys_def), final_angle(Ham, sol), lifetime(Ham, sol)*au2nseconds, final_diatom(Ham, sol)[1]])
    np.savetxt(tf.name, np.array(data))
    import pdb
    pdb.set_trace()
